+++
title = "Weekly #LinuxPhone Update (23/2022): Linux on Apple A7-A8X, flaws in U-Boot and the number 100"
date = "2022-06-12T13:50:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Mobile GNOME Shell","Linux on iPhone","Linux on iPad","Sailfish OS","Phosh","Phoc","Notkia",]
categories = ["weekly update"]
authors = ["peter"]
+++

A new Manjaro Phosh Beta, videos about Phosh on tablets and its upcoming gestures, and another Ubuntu Touch Q&A!

<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#47 Counting Items](https://thisweek.gnome.org/posts/2022/06/twig-47/).
* Sophie's Blog: [Apps: Attempt of a status report](https://blogs.gnome.org/sophieh/2022/06/08/apps-attempt-of-a-status-report/).
* Christian F.K. Schaller: [How to get your application to show up in GNOME Software](https://blogs.gnome.org/uraeus/2022/06/10/how-to-get-your-application-to-show-up-in-gnome-software/).
* Space Penguin: [Introduction](https://melix99.wordpress.com/2022/06/07/introduction/). _Telegrand is a splendid GTK4/libadwaita chat client, so I'm positive Marco will be able to greatly support Fractal's development!_

#### Plasma/Maui ecosystem

* Nate Graham: [This week in KDE: the Analog Clock changes color](https://pointieststick.com/2022/06/10/this-week-in-kde-the-analog-clock-changes-color/).
* KDE Community: [KDE Gear 22.04.2](https://kde.org/announcements/gear/22.04.2/).
* Adam Szopa: [KDE Goals Retrospective: Apps](https://szopa.org.pl/kde/2022/06/10/KDE-Goals-Retrospective-Apps.html).
* flyingcakes: [Community Bonding GSoC 2022 - post #2](https://snehit.dev/posts/kde/gsoc-22/community-bonding/).
* Adam Szopa: [Akademy 2022 Call for Participation](https://szopa.org.pl/kde/2022/06/10/Akademy-Call-for-Participation.html). _Only a very few hours left!_
* The Gcompris Ivy: [The journey from Season of KDE to Google Summer of Code 2022](https://thegcomprisivy.wordpress.com/2022/06/10/the-journey-from-season-of-kde-to-google-summer-of-code-2022/).

#### Ubuntu Touch
_See the Community Q&A video below!_

#### Distro news
* [Release Beta 24 · manjaro-pinephone/phosh](https://github.com/manjaro-pinephone/phosh/releases/tag/beta24).
* [This month in Manjaro (May 2022)](https://blog.strits.dk/this-month-in-manjaro-may-2022/). _Nice report!_

#### U-Boot
* The Hacker News.com: [Unpatched Critical Flaws Disclosed in U-Boot Bootloader for Embedded Devices](https://thehackernews.com/2022/06/unpatched-critical-flaws-disclosed-in-u.html).

#### Linux and Mesa

* Phoronix: [Panfrost's Initial Arm Mali Valhall Support Sent In To DRM-Next For Linux 5.20](https://www.phoronix.com/scan.php?page=news_item&px=DRM-Misc-Next-For-Linux-5.20).
* Phoronix: [Linux 5.20 + Mesa 22.2 To Allow Conformant Mali G57 OpenGL ES 3.1 Support](https://www.phoronix.com/scan.php?page=news_item&px=Mali-G57-Conformant-GLES-3.1).


### Worth noting
* If having to use DUO Mobile 2FA holds you back from switching to Mobile Linux, [this section on the Mobian Wiki may by of interest to you](https://wiki.mobian.org/doku.php?id=howto:security#duo-mobile-2fa).
* u/textuist: [Any Interest In A PineFlipPhone? : PINE64official](https://www.reddit.com/r/PINE64official/comments/va4s5i/any_interest_in_a_pineflipphone/). _I don't think that this is at all likely to ever happen, but it's a fun idea._
* Megi had [more fun with the PinePhone Pro camera](https://megous.com/dl/tmp/ppp-wificam2.mp4).

### Worth reading

#### Porting Linux to A7, A8 and A8X
* Konrad Dybcio: [Linux on A7-A8X](https://konradybcio.pl/linuxona7/).

#### Fluff
* Purism: [The Ultimate Guide to Free Software](https://puri.sm/posts/the-ultimate-guide-to-free-software/).

### Worth watching

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 118](https://www.youtube.com/watch?v=U8CwOoQK5M4). _Finally another nice update by Florian and Alfred! OTA 23 is turning the Pixel 3a into the best supported UBports device of all time, including wireless display support. Help is needed to test the release candidate of 16.04 OTA 23. Also, 20.04 (Focal Fosssa) is getting into a state where more people can help out. Also: News on VoLTE (it's very difficult). Also, a long Q&A!_
* @fredldotme: [Here’s a short demo of Aethercast and Desktop Mode working on a Pixel 3a with a Amazon FireTV Lite Stick.](https://twitter.com/fredldotme/status/1535952567859040256). _Impressive!_

#### Phosh on a tablet
* Linux Stuff: [Mobian on Chuwi Hi10 GO (Linux Tablet)](https://www.youtube.com/watch?v=SbaSwyGWMCA). _If you're wondering why Netflix, Brave and all that stuff works relatively effortlessly: This is an x86_64 tablet._

#### Gestures in Phosh
* Martijn Braam: [Phosh 0.20 Beta 1 on a PinePhone](https://www.youtube.com/watch?v=n7dBqWlzt-A). _Great job! P.S.: Night light has been working for a while on PinePhone!_
* LINMOB: [The new gesture-based navigation of Phosh 0.20](https://tilvids.com/w/sZeifcSyyvbYXXzhTts81T).

#### KDE Goals
* [KDE Goals interview: "All about the Apps" with Aleix Pol](https://tube.kockatoo.org/w/n4sFdeJ1BrmZwuHRyAwzpo).


### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!


### 100!
Thank you for reading this 100th iteration (78 iterations of LinBits + 22 under the new name) of my Weekly Update on Linux Phones! I doubt I'll make it to 200 – at some point, one has to move on; hopefully someone else will take over! Note: This does not mean that I'll quit soon, I aim to continue this until reaching 2<sup>7</sup> for now.

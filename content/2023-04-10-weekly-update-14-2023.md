+++
title = "Weekly GNU-like Mobile Linux Update (14/2023): PineTab V and Happy Easter!"
draft = false
date = "2023-04-10T22:00:00Z"
[taxonomies]
tags = ["Sailfish OS","Phosh","GNOME Mobile","Nemo Mobile","PineTab V","Mobian"]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "supported by [cat-master21](https://github.com/cat-master21) (+ friendly assistance from plata's awesome script)"
+++

Also: Phosh 0.26.0, Lomiri in Fedora, newer GNOME Shell with mobile patches in postmarketOS, mainlining progress and more!.
<!-- more -->
_Commentary in italics._

### Hardware
- PINE64: [PineTab-V and PineTab2 launch](https://www.pine64.org/2023/04/10/pinetab-v-and-pinetab2-launch/)

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#90 Enabling Feedback](https://thisweek.gnome.org/posts/2023/04/twig-90/)
- jrb: [Crosswords 0.3.8: Change Management](https://blogs.gnome.org/jrb/2023/04/02/crosswords-0-3-8-change-management/)
- Purism community: [Squeekboard release 1.22.0](https://forums.puri.sm/t/squeekboard-release-1-22-0/19884)
- phosh.mobi: [Phosh 0.26.0](https://phosh.mobi/releases/rel-0.26.0/)
#### Plasma ecosystem	
- Nate Graham: [This week in KDE: All about the apps](https://pointieststick.com/2023/04/08/this-week-in-kde-all-about-the-apps-2/)
- KDE Announcements: [KDE Ships Frameworks 5.105.0](https://kde.org/announcements/frameworks/5/5.105.0/)
- KDE Announcements: [KDE Plasma 5.27.4, Bugfix Release for April](https://kde.org/announcements/plasma/5/5.27.4/)
- Volker Krause: [Branching KDE PIM for the final phase of the Qt 6 port](https://www.volkerkrause.eu/2023/04/07/kde-pim-kf6-branching.html)
- k3yss: [Season of KDE Blog #2](https://k3yss.github.io/posts/sok_blog2/)
- Nicolas Fella: [Month two as KDE Software Platform Engineer](https://nicolasfella.de/posts/softwareplatform-month-2/)
- Carl Schwan: [Accessible icon-only buttons in QML](https://carlschwan.eu/2023/04/06/accessible-icon-only-buttons-in-qml/)
- Qt blog: [Qt for Python: the new 6.5 version is out!](https://www.qt.io/blog/qt-for-python-6.5)

#### Sailfish OS
- Sailfish OS Forum: [Sailfish Community News, 6th April, 5G](https://forum.sailfishos.org/t/sailfish-community-news-6th-april-5g/15311)

#### Distributions
- Fedora (Terra): [Lomiri is desktop in Terra for Fedora](https://repology.org/project/lomiri/versions) *Though still very buggy and experimental!*
- postmarketOS: [Gnome Shell mobile patches](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3996) on postmarketOS now include verdre's latest commits for lockscreen calls, PIN entry, Gnome 44, quick settings styling, and many many stability improvements.

#### Linux
- PinePhone Pro: [Javier mainlined the PPP's display/touchscreen support](https://lore.kernel.org/all/20230328073309.1743112-1-javierm@redhat.com/)
- phone-devel: [[PATCH v5 5/5] arm64: dts: qcom: sdm845-shift-axolotl: enable SLPI](http://lore.kernel.org/phone-devel/20230406173148.28309-6-me@dylanvanassche.be/)
- phone-devel: [[PATCH v5 4/5] arm64: dts: qcom: sdm845-oneplus: enable SLPI](http://lore.kernel.org/phone-devel/20230406173148.28309-5-me@dylanvanassche.be/)

#### Matrix

#### Stack
- Phoronix: [Mesa 23.0.2 Released With Dozens Of Fixes](https://www.phoronix.com/news/Mesa-23.0.2-Released)
- Phoronix: [Hangover 0.8.5 Released For Running Windows Apps/Games With Wine On AArch64/POWER](https://www.phoronix.com/news/Wine-Hangover-0.8.5)

### Worth noting
* [Personne: "8 years ago to this day, the f…" - Mastodon](https://mastodon.social/@per_sonne/110174708596118862)
* [Dimitris Kardarakos: "A simple metronome script for …" - FLOSS.social](https://floss.social/@dimitrisk/110168780753889216)
* [Guido Günther: "Some of the styling for #phosh's long button pres…" - Librem Social](https://social.librem.one/@agx/110169411107984130)
* [Supervisor:\~$ :idle:: "New Gnome-Mobile update includ…" - Fosstodon](https://fosstodon.org/@Supervisor/110169169965284368)
* [Chris: "I've been playing around a lot…" - Fosstodon](https://fosstodon.org/@kop316/110164111841338498)
* [LINux on MOBile: "Just noticed https://linuxphon…" - Fosstodon](https://fosstodon.org/@linmob/110157127762158800)

### Worth reading
- LWN.net: [Mobian: bringing Debian to mobile devices](https://lwn.net/SubscriberLink/928211/e2a857ca494990ea/)
- Laoblog: [Pinephone review: Nemo Mobile](https://blog.libero.it/Laoblog2/16591126.html)
- John_Andrew for Purism community: [Another Unsolicited Review (hold onto your hats)](https://forums.puri.sm/t/another-unsolicited-review-hold-onto-your-hats/19889)
- /u/Galaxiomatic: [2 years later...](https://www.reddit.com/r/PINE64official/comments/12garkn/2_years_later/)
- /u/multilinear2: [Writeup: Pinephone Pro w/keyboard running Gentoo + sway (tips, not quite instructions)](https://www.reddit.com/r/pinephone/comments/129qqhx/writeup_pinephone_pro_wkeyboard_running_gentoo/)

### Maybe Worth watching
- Caffeine: [A quick look at Gnome Mobile](https://www.youtube.com/watch?v=evnpVcUyL38)
- mikehenson5: [PinePhone - Phosh - Replace a desktop icon](https://www.youtube.com/watch?v=rbEiKR6wwYE)
- mikehenson5: [PinePhone - Phosh - Arch Linux - Update the ADSP version running the Biktorgj firmware](https://www.youtube.com/watch?v=XsCpmkE5ttg)
- Alexander Black: [Librem 5 ,version 1 Evergreen.](https://www.youtube.com/watch?v=4SzL55LTjU0)
- Cloud: [Pinephone battery test. Pmos with sxmo.](https://www.youtube.com/watch?v=2vRAF1nQVRQ)
- My Happy Abby: [Linux Mobile - Featuring Ubuntu Touch on a Google Pixel 3A XL (SARGO) device](https://www.youtube.com/watch?v=zTLU4sSLlB8)
- Frickelfieber: [Install and setup Waydroid in Ubuntu - Use Android on your Linux tablet!](https://www.youtube.com/watch?v=BqP__bJGIgM)
- Ino Xavier: [Fairphone4 with WayDroid on Ubuntu Touch 20.04 from UBports](https://www.youtube.com/watch?v=4Zr7543P8dU)
- Continuum Gaming: [Continuum Gaming E359: Sailfish OS – Micro G might not work for you even so it is installed](https://www.youtube.com/watch?v=-w3e2XrprjE)
- Jolla Devices: [Nokia N9 chinese: how to find the language selection and set your language.](https://www.youtube.com/watch?v=65ji-iMFOfA)
- Twinntech: [Pinephone Is it still a flashlight in 2023](https://www.youtube.com/watch?v=SvKM_DyyAg4) _Don't engage!_

### Thanks
Huge thanks to adraic, ptrshb, KDG and again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__


+++
title = "Convergent Windows for KDE Plasma"
date = "2023-02-11T15:00:00Z"
updated = "2023-02-11T15:00:00Z"
draft = false
[taxonomies]
tags = ["convergence", "Plasma", "KDE"]
categories = ["software"]
authors = ["Plata"]
[extra]
edit_post_with_images = true
update_note = ""
+++

KDE Plasma windows work well on large screens and can also be tailored for mobile devices.
But what if an external screen is connected to a mobile device?
<!-- more -->

When used on the desktop, Plasma windows have window decorations with titlebar buttons and can be resized and moved around.
With [Plasma Phone Settings](https://invent.kde.org/plasma-mobile/plasma-phone-settings), the windows do not have window
decorations and are always maximized. This makes perfect sense on small mobile screens and is exactly what you would expect in
Plasma Mobile.

However, if an external screen is connected to the mobile device, you will want to use it in desktop mode (i.e. convergence).
There is an idea to add a specific setting in Plasma Mobile to [toggle convergence mode](https://invent.kde.org/plasma/plasma-mobile/-/issues/231).

I thought to myself "there must be a way to achieve this automatically" and here it is:

\*\*drumroll\*\*

[Convergent Windows](https://store.kde.org/p/1985909/) is a KWin script which
- removes window decorations and maximizes windows on the first screen (i.e. mobile device)
- shows window decorations and generally uses the default behavior on all other screens (i.e. external screens)

<video width="960" height="360" controls>
  <source src="convergent-windows.mp4" type="video/mp4">
</video>

Currently, there's a known limitation with GTK apps: Some GTK apps draw the titlebar buttons inside the window. Those are still visible even if the window decorations are removed.

Convergent Windows can be used with both Plasma Desktop and Plasma Mobile. Note, however, that distributions which provide
Plasma Mobile images install also Plasma Phone Settings. This might overrule the desired behavior.

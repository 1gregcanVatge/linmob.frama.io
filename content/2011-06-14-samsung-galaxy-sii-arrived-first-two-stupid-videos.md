+++
title = "Samsung Galaxy SII arrived: First two stupid videos."
aliases = ["2011/06/14/samsung-galaxy-sii-arrived-first-two-stupid-videos.html"]
author = "peter"
date = "2011-06-14T13:09:00Z"
layout = "post"
[taxonomies]
tags = ["Android 2.3 Gingerbread", "Samsung", "Samsung Galaxy SII", "Unboxing", "walkthrough"]
categories = ["hardware", "impressions", "videos", "shortform"]
authors = ["peter"]
+++

This morning my SGS2 that I received from trnd.com for testing arrived. I immidiately shot two videos, but they are both quite bad. However, I feel like sharing, so enjoy!

Unboxing w/ size comparison to Acer Stream:

* LINMOBnet: [Samsung Galaxy S2 (GT-i9100) unboxing, size comparison with Acer Stream](https://www.youtube.com/watch?v=HvnBjghMJA4)

Second video - a walkthrough

* LINMOBnet: [Samsung Galaxy S2 - Quick walkthrough](https://www.youtube.com/watch?v=qFCrweiy-xg)
